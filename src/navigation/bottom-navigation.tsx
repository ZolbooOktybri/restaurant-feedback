import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { HomeScreen } from '../screens/home/home';
import { Favorite } from '../screens/favorite/favorite';
import { Profile } from '../screens/profile/profile';
import { BottomTabBar } from './bottom-tab-bar';
import { Location } from '../screens/location/location';

const Tab = createBottomTabNavigator();

export const BottomBar = () => {
   return (
      <Tab.Navigator tabBar={(props) => <BottomTabBar {...props} />}>
         <Tab.Screen name={'Home'} component={HomeScreen} />
         <Tab.Screen name={'Favorite'} component={Favorite} />
         <Tab.Screen name={'Profile'} component={Profile} />
         <Tab.Screen name={'Location'} component={Location} />
      </Tab.Navigator>
   )
}