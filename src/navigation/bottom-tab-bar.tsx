import React from 'react';
import { TouchableOpacity, Dimensions } from 'react-native';
import { Box } from '../components';
const { width } = Dimensions.get('window');
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { HearthIcon, HomeIcon, LocationIcon, ProfileIcon } from '../components/icons';

const tabIcons: any = {
   Home: HomeIcon,
   Favorite: HearthIcon,
   Profile: ProfileIcon,
   Location: LocationIcon
};

export const BottomTabBar: React.FC<any> = ({
   state,
   descriptors,
   navigation,
}) => {
   const focusedOptions = descriptors[state.routes[state.index].key].options;

   if (focusedOptions.tabBarVisible === false) {
      return null;
   }

   const insets = useSafeAreaInsets();


   return (
      <Box
         flexDirection="row"
         width="100%"
         height={65 + insets.bottom}
         position="relative"
         role={'white200'}
      >
         {state.routes.map((route: any, index: any) => {
            const CustomIcon = tabIcons[route.name];
            const { options } = descriptors[route.key];

            const isFocused = state.index === index;

            const onPress = () => {
               const event = navigation.emit({
                  type: 'tabPress',
                  target: route.key,
                  canPreventDefault: true,
               });

               if (!isFocused && !event.defaultPrevented) {
                  navigation.navigate(route.name);
               }
            };

            const onLongPress = () => {
               navigation.emit({
                  type: 'tabLongPress',
                  target: route.key,
               });
            };

            return (
               <TouchableOpacity
                  accessibilityRole="button"
                  key={route.name}
                  accessibilityState={isFocused ? { selected: true } : {}}
                  accessibilityLabel={options.tabBarAccessibilityLabel}
                  testID={options.tabBarTestID}
                  onPress={onPress}
                  onLongPress={onLongPress}
                  style={{
                     flex: 1,
                     height: 65,
                     justifyContent: 'center',
                     alignItems: 'center',
                  }}
               >
                  <CustomIcon color={isFocused ? '#FA4A0C' : '#ADADAF'} />
               </TouchableOpacity>
            );
         })}
      </Box>
   );
};
