import React from 'react';
import Svg, { Path } from 'react-native-svg';

interface Props {
   width?: number | string;
   heigth?: number | string;
   color?: string;
}

export const HearthIcon: React.FC<Props> = ({ width = 24, heigth = 24, color = "#ADADAF" }) => {
   return (
      <Svg width={width} height={heigth} viewBox="0 0 24 24" fill="none" >
         <Path
            d="M20.84 4.61a5.5 5.5 0 00-7.78 0L12 5.67l-1.06-1.06a5.501 5.501 0 00-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 000-7.78v0z"
            stroke={color}
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
         />
      </Svg>
   )
}