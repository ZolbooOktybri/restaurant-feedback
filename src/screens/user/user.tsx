import React, { useState } from 'react';
import Auth from '@react-native-firebase/auth'
import { KeyboardAvoidingView, ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Box, Stack, Text, BackgroundImage, Border, Button, Queue, Spacing } from '../../components';
import { NavigationRoutes } from '../../navigation/navigation-route';
import { SignIn } from './sign-in';
import { SignUp } from './sign-up';
import { useCollection } from '../../hooks/firebase';

const logo = require('../../assets/burger-logo.png');

const SubButton = ({ value, selected, onPress }: any) => {
   return (
      <Button category={'text'} onPress={() => onPress()}>
         <Stack alignItems={'center'} size={2}>
            <Text size={18} weight={'600'} role={'black'}>{value}</Text>
            <Border radius={5}>
               <Box width={10} height={10} role={selected ? 'primary' : 'white300'} />
            </Border>
         </Stack>
      </Button>
   )
}

export const User = () => {
   const navigation = useNavigation();
   const users = useCollection('users');
   const [isSignIn, setIsSignIn] = useState(true);
   const [email, setEmail] = useState<string | null>(null);
   const [pass, setPass] = useState<string | null>(null);
   const [rePass, setRePass] = useState<string | null>(null);

   const signUp = () => {
      if (email && pass && rePass)
         if (pass == rePass)
            Auth().createUserWithEmailAndPassword(email, pass)
               .then(async ({ user }: any) => {
                  console.log(user.uid)

                  await users.updateRecord(user.uid, { uid: user.uid, firstName: 'Edit me', secondName: 'Edit me', profile: '' });
                  navigation.navigate(NavigationRoutes.MainRoot);
               });
   };

   const signIn = () => {
      if (email && pass)
         Auth().signInWithEmailAndPassword(email, pass)
            .then(() => {
               navigation.navigate(NavigationRoutes.MainRoot);
            });
   }

   return (
      <SafeAreaView style={{ flex: 1 }}>
         <Box flex={1} role={'white100'}>
            <ScrollView stickyHeaderIndices={[1]}>
               <Box width={'100%'} height={200} role={'white300'} justifyContent={'center'} alignItems={'center'}>
                  <BackgroundImage source={logo} width={150} height={150} />
               </Box>
               <Border borderBottomLeftRadius={30} borderBottomRightRadius={30}>
                  <Box width={'100%'} height={80} role={'white300'} justifyContent={'center'}>
                     <Queue justifyContent={'center'} size={30}>
                        <SubButton value={'Sign-in'} selected={isSignIn ? true : false} onPress={() => setIsSignIn(true)} />
                        <SubButton value={'Sign-up'} selected={isSignIn ? false : true} onPress={() => setIsSignIn(false)} />
                     </Queue>
                  </Box>
               </Border>
               {
                  isSignIn ?
                     <SignIn setEmail={setEmail} setPass={setPass} /> :
                     <SignUp setEmail={setEmail} setPass={setPass} setRePass={setRePass} />
               }
            </ScrollView>
            <KeyboardAvoidingView style={{ width: '100%' }}
               keyboardVerticalOffset={80}
               behavior={'padding'}>
               <Spacing ph={10}>
                  <Button borderRadius={30} width={'100%'} onPress={() => isSignIn ? signIn() : signUp()}>
                     <Text size={17} weight={'600'} role={'white300'}>{isSignIn ? 'Sign-in' : 'Sign-up'}</Text>
                  </Button>
               </Spacing>
            </KeyboardAvoidingView>
         </Box>
      </SafeAreaView>
   )
}