import React from 'react';
import { connectSearchBox } from 'react-instantsearch-native';
import { SearchInput } from './search-input';

const SearchBox = ({ currentRefinement, refine }: any) => {
   return (
      <SearchInput
         value={currentRefinement}
         onChangeText={(text: any) => refine(text)}
      />
   )
};

export const CustomSearchBox = connectSearchBox(SearchBox);