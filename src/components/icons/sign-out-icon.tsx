import React from 'react';
import Svg, { Path } from 'react-native-svg';

interface Props {
   width?: number | string;
   heigth?: number | string;
   color?: string;
}

export const SignOutIcon: React.FC<Props> = ({ width = 24, heigth = 24, color = "#ADADAF" }) => {
   return (
      <Svg width={width} height={heigth} viewBox="0 0 24 24" fill="none">
         <Path
            d="M19 21h-9a2 2 0 01-2-2v-4h2v4h9V5h-9v4H8V5a2 2 0 012-2h9a2 2 0 012 2v14a2 2 0 01-2 2zm-7-5v-3H3v-2h9V8l5 4-5 4z"
            fill={color}
         />
      </Svg>
   )
}