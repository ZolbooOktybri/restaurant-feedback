export enum NavigationRoutes {
   MainRoot = 'MainRoot',
   Splash = 'Splash',
   User = 'User',
   Review = 'Review',
   SuccessfullySubmitted = 'SuccessfullySubmitted',
   EditProfile = 'EditProfile',
}
export interface NavigationPayload<T> {
   props: T;
}
export type NavigatorParamList = {
   [NavigationRoutes.User]: NavigationPayload<any>;
   [NavigationRoutes.Splash]: NavigationPayload<any>;
   [NavigationRoutes.Review]: NavigationPayload<any>;
   [NavigationRoutes.MainRoot]: NavigationPayload<any>;
   [NavigationRoutes.EditProfile]: NavigationPayload<any>;
   [NavigationRoutes.SuccessfullySubmitted]: NavigationPayload<any>;
};