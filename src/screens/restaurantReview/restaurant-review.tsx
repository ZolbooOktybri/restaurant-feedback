import React from 'react';
import MapView, { Marker } from 'react-native-maps';
import { SafeAreaView, ScrollView } from 'react-native';
import Auth from '@react-native-firebase/auth';

import { BackgroundImage, Box, Button, Spacing, Stack, Text } from '../../components';
import { HearthIcon } from '../../components/icons';
import { useDocument } from '../../hooks/firebase';
import { useNavigation } from '@react-navigation/core';
import { NavigationRoutes } from '../../navigation/navigation-route';

const ReviewText = ({ text, label }: any) => {
   return (
      <Stack size={4}>
         <Text size={15} lineHeight={18} weight={'600'} opacity={0.4} role={'black'} width={'100%'}>{label}</Text>
         <Text size={17} lineHeight={18} weight={'600'} opacity={0.8} role={'black'} width={'100%'}>{text}</Text>
      </Stack>
   )
}

export const RestaurantReview = ({ route }: any) => {
   const navigation = useNavigation();
   const uid = Auth().currentUser?.uid;
   const { id, coordinate } = route.params;
   const data: any = useDocument(`restaurants/${id}`).doc;
   const favorite = useDocument(`users/${uid}/favorite/${id}`);

   const addFavorite = () => {
      if (favorite.doc)
         favorite.deleteRecord();
      else
         favorite.updateRecord({ id, name: data.name, rate: data.rate, coordinate });
         
      navigation.navigate(NavigationRoutes.SuccessfullySubmitted);
   }

   return (
      <SafeAreaView style={{ flex: 1 }}>
         <ScrollView>
            <MapView
               style={{ width: '100%', height: 250 }}
               initialRegion={{
                  latitude: coordinate.latitude,
                  longitude: coordinate.longitude,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421,
               }}
            >
               <Marker
                  coordinate={{
                     latitude: coordinate.latitude,
                     longitude: coordinate.longitude,
                  }}
               >
                  <BackgroundImage source={require('../../assets/marker.png')} width={20} height={20} />
               </Marker>
            </MapView>
            <Spacing ph={6} mt={4} mb={20}>
               <Stack size={6}>
                  <Box width={'100%'} alignItems={'flex-end'}>
                     <Button onPress={() => addFavorite()} category={'text'}>
                        <HearthIcon color={favorite.doc ? '#FA4A0C' : '#ADADAF'} />
                     </Button>
                  </Box>
                  <ReviewText text={data?.name} label={'name'} />
                  <ReviewText text={data?.whatDidYouOrder} label={'What did you order?'} />
                  <ReviewText text={data?.qualityOfFood} label={'Quality of food'} />
                  <ReviewText text={data?.portionSize} label={'Portion size'} />
                  <ReviewText text={data?.easeOfOrdering} label={'Ease of ordering'} />
                  <ReviewText text={data?.service} label={'Service'} />
                  <ReviewText text={data?.cleanliness} label={'Cleanliness'} />
                  <ReviewText text={data?.overallValue} label={'Overall value'} />
                  <ReviewText text={data?.comment} label={'Comments'} />
                  <ReviewText text={data?.rate} label={'Rate'} />
               </Stack>
            </Spacing>
         </ScrollView>
      </SafeAreaView >
   )
}