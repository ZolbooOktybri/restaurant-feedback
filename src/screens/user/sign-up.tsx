import React, { useState } from 'react';
import { Box, Spacing, Stack } from '../../components';
import { Input } from '../../components/core/input';

interface Props {
   setEmail: Function,
   setPass: Function,
   setRePass: Function,
}

export const SignUp: React.FC<Props> = ({ setEmail, setPass, setRePass }) => {

   return (
      <Box flex={1}>
         <Spacing ph={10} mt={13}>
            <Stack size={10}>
               <Input label={'Email address'} underLine onChangeText={(text: any) => setEmail(text)} />
               <Input label={'Password'} underLine type={'password'} onChangeText={(text: any) => setPass(text)} />
               <Input label={'Re-Password'} underLine type={'password'} onChangeText={(text: any) => setRePass(text)} />
            </Stack>

         </Spacing>
      </Box>
   )
}