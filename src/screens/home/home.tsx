import React, { useState } from 'react';
import _ from 'lodash';
import { FlatList, SafeAreaView } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import { Box, RestaurantCard, SearchInput, Spacing, Stack, Text } from '../../components';
import { useCollection } from '../../hooks/firebase';
import { InstantSearch, connectHits, connectRefinementList } from 'react-instantsearch-dom';
import { algoliaIndex, searchClient } from '../../algolia';
import { CustomSearchBox } from '../../components/searchBox';
import { NavigationRoutes } from '../../navigation/navigation-route';

const Hit = ({ hits }: any) => {
   // console.log(hits)
   return (
      <Stack>
         {hits.map((hit: any) => (
            <Text size={10} key={hit.objectID}>{hit.name}</Text>
         ))}
      </Stack>
   )
};

const CustomHits = connectHits(Hit);

// const RefinementList = ({ items }: any) => (
//    <Stack>
//       {items.map((item: any) => (
//          <Box key={item.label}>
//             <Text size={10}>
//                {item.label} ({item.count})
//          </Text>
//          </Box>
//       ))}
//    </Stack>
// );
// const CustomRefinementList = connectRefinementList(RefinementList);

export const HomeScreen = () => {
   const restaurants = useCollection('restaurants').collection;
   const navigation = useNavigation();

   return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#F5F5F8' }}>
         <Spacing ph={6}>
            <Stack size={10}>
               <Text size={34} weight={'600'} role={'black'}>How was that Restaurant</Text>
               {/* <SearchInput onChangeText={(text: string) => console.log(text)} /> */}
               <FlatList
                  data={restaurants}
                  renderItem={({ item }) => (
                     <RestaurantCard
                        name={item.name}
                        rate={item.rate}
                        onPress={() => navigation.navigate(NavigationRoutes.Review, item)}
                     />
                  )}
                  keyExtractor={(_, index) => 'restaurant-' + index}
                  ItemSeparatorComponent={() => <Box height={20} />}
               />
               {/* <InstantSearch
                  searchClient={searchClient}
                  indexName={'restaurantFeedback'}
               >
                  <CustomSearchBox defaultRefinement={'the'} />
                  <CustomHits />
               </InstantSearch> */}
            </Stack>
         </Spacing>
      </SafeAreaView>
   )
}