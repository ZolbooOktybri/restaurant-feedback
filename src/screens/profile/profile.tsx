import React from 'react';
import { SafeAreaView } from 'react-native';
import { Border, Box, Button, Queue, Spacing, Stack, Text } from '../../components';
import { SignOutIcon } from '../../components/icons';
import Auth from '@react-native-firebase/auth';
import { useNavigation } from '@react-navigation/core';
import { NavigationRoutes } from '../../navigation/navigation-route';
import { useDocument } from '../../hooks/firebase';

const TextRender = ({ value, onPress, type }: any) => {
   return (
      <Queue alignItems={'center'} size={2}>
         <Text size={18} weight={'700'} role={'gray'}>{type} :</Text>
         <Button height={40} onPress={() => onPress()} category={'text'} size={'s'}>
            <Text size={30} weight={'700'} role={'black'}>{value}</Text>
         </Button>
      </Queue>
   )
}

export const Profile = () => {
   const navigation = useNavigation();
   const { uid }: any = Auth().currentUser;
   const userData: any = useDocument(`users/${uid}`).doc;

   return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#F5F5F8' }}>
         <Spacing ph={10}>
            <Spacing mb={10}>
               <Text size={34} weight={'600'} role={'black'}>My profile</Text>
            </Spacing>

            <Stack>
               <TextRender type={'firstName'} value={userData?.firstName} onPress={() => navigation.navigate(NavigationRoutes.EditProfile, { type: 'firstName' })} />
               <TextRender type={'secondName'} value={userData?.secondName} onPress={() => navigation.navigate(NavigationRoutes.EditProfile, { type: 'secondName' })} />
            </Stack>

         </Spacing>

         <Box position={'absolute'} bottom={0} left={40}>
            <Button
               borderRadius={17}
               onPress={() => {
                  Auth().signOut();
                  navigation.navigate(NavigationRoutes.User)
               }}
               category={'fill'}
               type={'gray'}
            >
               <Box width={120} height={50} justifyContent={'center'} alignItems={'center'}>
                  <Queue size={2} alignItems={'center'}>
                     <SignOutIcon color={'#FA4A0C'} />
                     <Text size={18} role={'black'}>Sign Out</Text>
                  </Queue>
               </Box>
            </Button>
         </Box>

      </SafeAreaView>
   )
}