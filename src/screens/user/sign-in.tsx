import React, { useState } from 'react';
import Auth from '@react-native-firebase/auth';
import { Box, Button, Spacing, Stack, Text } from '../../components';
import { Input } from '../../components/core/input';

interface Props {
   setEmail: Function,
   setPass: Function,
}

export const SignIn: React.FC<Props> = ({ setEmail, setPass }) => {

   return (
      <Box flex={1}>
         <Spacing ph={10} mt={13}>
            <Stack size={10}>
               <Input label={'Email address'} underLine onChangeText={(text: any) => setEmail(text)} />
               <Input label={'Password'} underLine type={'password'} onChangeText={(text: any) => setPass(text)} />
            </Stack>
            <Spacing mt={3}>
               <Button onPress={() => console.log('')} category={'text'}>
                  <Text size={17} weight={'600'}>Forgot password?</Text>
               </Button>
            </Spacing>
         </Spacing>
      </Box>
   )
}