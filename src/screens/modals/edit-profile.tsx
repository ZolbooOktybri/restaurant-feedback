import React, { useState } from 'react';
import {
   View,
   StyleSheet,
   Dimensions,
   TouchableOpacity,
} from 'react-native';
import { useNavigation } from '@react-navigation/core';
import Auth from '@react-native-firebase/auth';

import { Button, Border, Box, Queue, Spacing, Stack, Text, Input, SuccessIcon } from '../../components/';
import { useDocument } from '../../hooks/firebase';
const { width, height } = Dimensions.get('window');


export const EditProfile: React.FC<any> = ({ route }) => {
   const { type } = route.params;
   const { uid }: any = Auth().currentUser;
   const userData = useDocument(`users/${uid}`);
   const navigation = useNavigation();
   const [value, setValue] = useState('');

   const changeData = () => {
      if (value !== '') {
         userData.updateRecord({ [type]: value });
         navigation.goBack();
      }
   }

   return (
      <Box flex={1} justifyContent={'flex-end'} alignItems={'center'}>
         <TouchableOpacity
            style={StyleSheet.absoluteFill}
            onPress={() => navigation.goBack()}
         >
            <View
               style={[
                  StyleSheet.absoluteFill,
                  { backgroundColor: 'rgba(18,18,18,0.4)' },
               ]}
            ></View>
         </TouchableOpacity>

         <Border borderTopLeftRadius={30} borderTopRightRadius={30}>
            <Box width={width} height={height * 0.7} role={'white100'}>
               <Spacing mt={15} mb={15} mr={10} ml={10}>
                  <Stack size={6} alignItems={'center'}>
                     <Queue alignItems={'center'} size={2}>
                        <Text
                           size={20}
                           textAlign={'center'}
                           bold
                           role={'gray'}
                        >
                           Edit
                        </Text>
                        <Text
                           size={25}
                           textAlign={'center'}
                           bold
                           role={'black'}
                        >
                           {type}
                        </Text>
                     </Queue>
                     <Input onChangeText={(text: any) => setValue(text)} underLine width={width * 0.8} />

                     <Queue justifyContent={'center'}>
                        <Button width={200} onPress={() => changeData()} status="default" borderRadius={30}>
                           <Text
                              width={150}
                              size={20}
                              role={'white100'}
                              textAlign={'center'}
                              bold
                           >
                              Edit
                           </Text>
                        </Button>
                     </Queue>
                  </Stack>
               </Spacing>
            </Box>
         </Border>
      </Box>
   );
};