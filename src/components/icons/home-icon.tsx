import React from 'react';
import Svg, { Path } from 'react-native-svg';

interface Props {
   width?: number | string;
   heigth?: number | string;
   color?: string;
}

export const HomeIcon: React.FC<Props> = ({ width = 24, heigth = 24, color = "#ADADAF" }) => {
   return (
      <Svg width={width} height={heigth} viewBox="0 0 25 25" fill="none">
         <Path
            d="M13.596.554a1.55 1.55 0 00-2.192 0L.554 11.404a1.55 1.55 0 002.192 2.192l.454-.454V23.35a1.55 1.55 0 001.55 1.55h3.1a1.55 1.55 0 001.55-1.55v-3.1a1.55 1.55 0 011.55-1.55h3.1a1.55 1.55 0 011.55 1.55v3.1a1.55 1.55 0 001.55 1.55h3.1a1.55 1.55 0 001.55-1.55V13.142l.454.454a1.55 1.55 0 002.192-2.192L13.596.554z"
            fill={color}
         />
      </Svg>
   )
}