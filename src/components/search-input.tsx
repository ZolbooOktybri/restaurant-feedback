import React from 'react';
import { Input } from './core/input';
import { SearchIcon } from './icons';
import { Border, Box, Queue } from './layout';

export const SearchInput: React.FC<Props> = ({ width, height, onChangeText, value }) => {
   return (
      <Box width={width ? width : '100%'} height={height ? height : 60}>
         <Border radius={30}>
            <Box width={width ? width : '100%'} height={height ? height : 60} role={'white100'} justifyContent={'center'} alignItems={'center'}>
               <Queue size={3} alignItems={'center'}>
                  <SearchIcon />
                  <Input width={220} onChangeText={(text: any) => onChangeText(text)} placeholder={'Search'} value={value}/>
               </Queue>
            </Box>
         </Border>
      </Box>
   )
}

interface Props {
   width?: number | string;
   height?: number | string;
   onChangeText: Function;
   value?: string;
}