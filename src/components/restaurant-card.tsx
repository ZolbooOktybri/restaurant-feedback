import React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Text } from './core';
import { Border, Box, Queue } from './layout';

export const RestaurantCard: React.FC<Props> = ({ name, rate, onPress }) => {
   return (
      <TouchableOpacity onPress={() => onPress()}>
         <Border radius={25}>
            <Box width={'100%'} height={50} role={'lightGray'}>
               <Queue size={3} alignItems={'center'}>
                  <Border borderTopLeftRadius={25} borderBottomLeftRadius={25}>
                     <Box width={50} height={50} role={'primary'}></Box>
                  </Border>

                  <Text size={22} weight={'600'} role={'black'}>{name}</Text>

                  <Box position={'absolute'} right={20}>
                     <Text size={17} weight={'500'} role={'gray'}>{rate}</Text>
                  </Box>
               </Queue>
            </Box>
         </Border>
      </TouchableOpacity>
   )
}

interface Props {
   name: string;
   rate: string;
   onPress: Function
}