import React, { useEffect, useState } from 'react';
import { KeyboardAvoidingView, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native';
import Geolocation from 'react-native-geolocation-service'
import MapView, { Marker } from 'react-native-maps';
import _ from 'lodash';
import fireStore from '@react-native-firebase/firestore';

import { BackgroundImage, Button, Spacing, Stack, Text } from '../../components';
import { Input } from '../../components/core/input';
import { useCollection } from '../../hooks/firebase';
import { useNavigation } from '@react-navigation/core';
import { NavigationRoutes } from '../../navigation/navigation-route';

export const Location = () => {
   const [currentLocation, setCurrentLocation] = useState<any>({});
   const [feedback, setFeedback] = useState<any>({});
   const [pin, setPin] = useState<any>(null);
   const navigation = useNavigation()
   const then = (res: any) => {
      const id = res._documentPath._parts[1]
      fireStore().collection('restaurants').doc(id).set({ id }, { merge: true });
   }

   const restaurants = useCollection('restaurants', then);

   useEffect(() => {
      Geolocation.getCurrentPosition(
         (position) => setCurrentLocation(position),
         (error) => console.log(error.code, error.message),
      );
   }, []);

   const submit = () => {
      restaurants.addRecord({ ...feedback, coordinate: pin.latitude ? pin : currentLocation.coords });
      setFeedback({});
      navigation.navigate(NavigationRoutes.SuccessfullySubmitted)
   };

   return (
      <SafeAreaView style={{ flex: 1 }}>
         <ScrollView>
            <KeyboardAvoidingView style={{ height: 'auto' }} behavior={'padding'}>
               <MapView
                  style={{ width: '100%', height: 250 }}
                  followsUserLocation
                  showsUserLocation
                  onPress={(e) => setPin(e.nativeEvent.coordinate)}
               >
                  {
                     _.map(restaurants.collection, (obj: any) => (
                        <Marker
                           key={obj.name}
                           coordinate={{
                              latitude: obj.coordinate.latitude,
                              longitude: obj.coordinate.longitude,
                           }}>
                           <TouchableOpacity>
                              <BackgroundImage source={require('../../assets/marker.png')} width={20} height={20} />
                           </TouchableOpacity>
                        </Marker>
                     ))
                  }

                  {
                     pin ? (
                        <Marker
                           coordinate={{
                              latitude: pin.latitude,
                              longitude: pin.longitude,
                           }}
                        >
                           <BackgroundImage source={require('../../assets/marker.png')} width={20} height={20} />
                        </Marker>
                     ) : <></>
                  }

               </MapView>
               <Spacing ph={6} mt={4} mb={20}>
                  <Stack size={5}>
                     <Input value={feedback.name} label={'Name'} underLine onChangeText={(text: any) => setFeedback((feedback: any) => ({ ...feedback, name: text }))} />
                     <Input value={feedback.whatDidYouOrder} label={'What did you order?'} underLine onChangeText={(text: any) => setFeedback((feedback: any) => ({ ...feedback, whatDidYouOrder: text }))} />
                     <Input value={feedback.qualityOfFood} label={'Quality of food'} underLine onChangeText={(text: any) => setFeedback((feedback: any) => ({ ...feedback, qualityOfFood: text }))} />
                     <Input value={feedback.porstionSize} label={'Portion size'} underLine onChangeText={(text: any) => setFeedback((feedback: any) => ({ ...feedback, portionSize: text }))} />
                     <Input value={feedback.easeOfOrdering} label={'Ease of ordering'} underLine onChangeText={(text: any) => setFeedback((feedback: any) => ({ ...feedback, easeOfOrdering: text }))} />
                     <Input value={feedback.service} label={'Service'} underLine onChangeText={(text: any) => setFeedback((feedback: any) => ({ ...feedback, service: text }))} />
                     <Input value={feedback.cleanliness} label={'Cleanliness'} underLine onChangeText={(text: any) => setFeedback((feedback: any) => ({ ...feedback, cleanliness: text }))} />
                     <Input value={feedback.overallValue} label={'Overall value'} underLine onChangeText={(text: any) => setFeedback((feedback: any) => ({ ...feedback, overallValue: text }))} />
                     <Input value={feedback.comment} label={'Comments?'} underLine onChangeText={(text: any) => setFeedback((feedback: any) => ({ ...feedback, comment: text }))} />
                     <Input value={feedback.rate} label={'Rate'} underLine keyboardType={'phone-pad'} onChangeText={(text: any) => setFeedback((feedback: any) => ({ ...feedback, rate: text }))} />
                     <Button onPress={() => submit()} width={'100%'} borderRadius={30}>
                        <Text size={20} role={'white300'}>Submit</Text>
                     </Button>
                  </Stack>
               </Spacing>
            </KeyboardAvoidingView>
         </ScrollView>
      </SafeAreaView>
   )
}