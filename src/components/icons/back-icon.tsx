import React from 'react';
import Svg, { Path } from 'react-native-svg';

interface Props {
   width?: number | string;
   heigth?: number | string;
   color?: string;
}

export const BackIcon: React.FC<Props> = ({ width = 24, heigth = 24, color = "#ADADAF" }) => {
   return (
      <Svg width={width} height={heigth} viewBox="0 0 24 24" fill="none">
         <Path
            d="M15.535 3.515L7.05 12l8.485 8.485 1.415-1.414L9.878 12l7.072-7.071-1.415-1.414z"
            fill={color}
         />
      </Svg>
   )
}