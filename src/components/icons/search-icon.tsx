import React from 'react';
import Svg, { Path } from 'react-native-svg';

interface Props {
   width?: number | string;
   heigth?: number | string;
   color?: string;
}

export const SearchIcon: React.FC<Props> = ({ width = 24, heigth = 24, color = "#ADADAF" }) => {
   return (
      <Svg width={width} height={heigth} viewBox="0 0 20 20" fill="none">
         <Path
            d="M19 19l-4.35-4.35M17 9A8 8 0 111 9a8 8 0 0116 0z"
            stroke={color}
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
         />
      </Svg>
   )
}