import React from 'react';
import { SafeAreaView } from 'react-native';
import Auth from '@react-native-firebase/auth';
import { Box, RestaurantCard, Spacing, Stack, Text } from '../../components';
import { useCollection } from '../../hooks/firebase';
import { FlatList } from 'react-native-gesture-handler';

export const Favorite = () => {
   const user = Auth().currentUser;
   const { uid } = user || {};
   const favorite = useCollection(`users/${uid}/favorite`).collection;

   return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#F5F5F8' }}>
         <Spacing ph={6}>
            <Stack size={10}>
               <Text size={34} weight={'600'} role={'black'}>Your favorite restaurants</Text>

               <FlatList
                  data={favorite}
                  renderItem={({ item }) => (
                     <RestaurantCard
                        name={item.name}
                        rate={item.rate}
                        onPress={() => console.log('g')}
                     />
                  )}
                  keyExtractor={(_, index) => 'restaurant-' + index}
                  ItemSeparatorComponent={() => <Box height={20} />}
               />
            </Stack>
         </Spacing>
      </SafeAreaView>
   )
}