import React, { useEffect } from 'react';
import Auth from '@react-native-firebase/auth';
import { useNavigation } from '@react-navigation/core';
import { Box } from '../../components';
import { BackgroundImage } from '../../components/layout/background-image';
import { Border } from '../../components/layout/border';
import { NavigationRoutes } from '../../navigation/navigation-route';

const backgroundImage = require('../../assets/splash-background.png');
const logo = require('../../assets/burger-logo.png');

export const Splash = () => {
   const user = Auth().currentUser;
   const navigation = useNavigation();

   useEffect(() => {
      setTimeout(() => {
         if (user)
            navigation.navigate(NavigationRoutes.MainRoot);
         else
            navigation.navigate(NavigationRoutes.User);
      }, 2000)
   })

   return (
      <BackgroundImage source={backgroundImage} width={'100%'} height={'100%'}>
         <Box width={'100%'} height={'100%'} justifyContent={'center'} alignItems={'center'}>
            <Border radius={125}>
               <Box width={250} height={250} role={'white300'} justifyContent={'center'} alignItems={'center'}>
                  <BackgroundImage source={logo} width={150} height={150} />
               </Box>
            </Border>
         </Box>
      </BackgroundImage>
   )
}