import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationRoutes, NavigatorParamList } from './navigation-route';
import { Splash } from '../screens/splash/splash';
import { NavigationContainer } from '@react-navigation/native';
import { User } from '../screens/user/user';
import { BottomBar } from './bottom-navigation';
import { RestaurantReview } from '../screens/restaurantReview/restaurant-review';
import { HeaderLeft, HeaderMiddle } from '../components';
import { SuccessfullySubmitted } from '../screens/modals/successfully-submitted';
import { EditProfile } from '../screens/modals/edit-profile';

const Stack = createStackNavigator<NavigatorParamList>();

export const RouterContainer = () => {
   return (
      <NavigationContainer>
         <Stack.Navigator initialRouteName={NavigationRoutes.Splash}>
            <Stack.Screen name={NavigationRoutes.User} component={User} options={() => ({ headerShown: false, gestureEnabled: false })} />
            <Stack.Screen name={NavigationRoutes.Splash} component={Splash} options={() => ({ headerShown: false })} />
            <Stack.Screen name={NavigationRoutes.MainRoot} component={BottomBar} options={() => ({ headerShown: false })} />
            <Stack.Screen
               name={NavigationRoutes.Review}
               component={RestaurantReview}
               options={({ navigation }) => ({
                  headerLeft: () => <HeaderLeft />,
                  headerTitle: () => <HeaderMiddle title={'Restaurant'} />,
                  headerRight: () => <></>,
               })}
            />
            <Stack.Screen
               name={NavigationRoutes.SuccessfullySubmitted}
               component={SuccessfullySubmitted}
               options={() => ({
                  headerShown: false,
                  cardStyle: { backgroundColor: 'transparent' },
                  cardOverlayEnabled: true,
                  cardStyleInterpolator: ({ current: { progress } }) => ({
                     cardStyle: {
                        opacity: progress,
                     },
                  }),
               })}
            />
            <Stack.Screen
               name={NavigationRoutes.EditProfile}
               component={EditProfile}
               options={() => ({
                  headerShown: false,
                  cardStyle: { backgroundColor: 'transparent' },
                  cardOverlayEnabled: true,
                  cardStyleInterpolator: ({ current: { progress } }) => ({
                     cardStyle: {
                        transform: [{ translateY: progress }],
                     },
                  }),
               })}
            />
         </Stack.Navigator>
      </NavigationContainer>
   )
}