import React from 'react';
import {
   View,
   StyleSheet,
   Dimensions,
   TouchableOpacity,
} from 'react-native';
import { useNavigation } from '@react-navigation/core';

import { Box, Queue, Spacing, Stack } from '../../components/layout';
import { Text } from '../../components/core/text';
import { Button, Border } from '../../components';
import { SuccessIcon } from '../../components/icons';

export const SuccessfullySubmitted: React.FC<any> = () => {
   const navigation = useNavigation();

   return (
      <Box flex={1} justifyContent={'center'} alignItems={'center'}>
         <TouchableOpacity
            style={StyleSheet.absoluteFill}
            onPress={() => navigation.goBack()}
         >
            <View
               style={[
                  StyleSheet.absoluteFill,
                  { backgroundColor: 'rgba(18,18,18,0.4)' },
               ]}
            ></View>
         </TouchableOpacity>

         <Border radius={8}>
            <Box width={Dimensions.get('window').width * 0.9} role={'white100'}>
               <Spacing mt={15} mb={15} mr={10} ml={10}>
                  <Stack size={6} alignItems={'center'}>
                     <SuccessIcon />
                     <Text
                        size={30}
                        textAlign={'center'}
                        bold
                        role={'black'}
                     >
                        Successfully submitted
                     </Text>
                     <Queue justifyContent={'center'}>
                        <Button width={200} onPress={() => navigation.goBack()} status="default" borderRadius={30}>
                           <Text
                              width={150}
                              size={20}
                              role={'white100'}
                              textAlign={'center'}
                              bold
                           >
                              Back to Home
                           </Text>
                        </Button>
                     </Queue>
                  </Stack>
               </Spacing>
            </Box>
         </Border>
      </Box>
   );
};