import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Text } from './core';
import { BackIcon } from './icons';
import { Box } from './layout';

interface Props {
   title?: string;
}

export const HeaderLeft: React.FC<Props> = () => {
   const navigation = useNavigation();

   return (
      <Box width={'100%'} height={40} justifyContent={'center'} alignItems={'center'}>
         <Box position={'absolute'} left={20}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
               <BackIcon />
            </TouchableOpacity>
         </Box>
      </Box>
   )
}

export const HeaderMiddle: React.FC<Props> = ({ title }) => {
   return (
      <Box width={'100%'} height={40} justifyContent={'center'} alignItems={'center'}>
         <Text size={24} weight={'600'} role={'black'}>{title}</Text>
      </Box>
   )
}

export const HeaderRight: React.FC<Props> = ({ title }) => {
   return (
      <Box width={'100%'} height={40} justifyContent={'center'} alignItems={'center'}>
         <Text size={24} weight={'600'} role={'black'}>{title}</Text>
      </Box>
   )
}