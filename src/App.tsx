import React from 'react';
import { SafeAreaView } from 'react-native';
import { RouterContainer } from './navigation';

const App = () => {
  return (
    <RouterContainer />
  );
};

export default App;
